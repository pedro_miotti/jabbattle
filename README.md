# Jab Battle

----

Jogo criado para a aula de **Projetos Interdiciplinares 1** da faculdade ESPM com o professor Carlos Rafael, simples jogo de Boxe usando Phaser e NodeJS.


**Integrantes do grupo :**

Pedro Miotti,
Thiago Miotti Arribamar,
Guilherme Frizo,
Pedro Pastelar,
Lucca Pascote,
Rafael Ruthledge,


Para rodar o jogo em sua maquina :

- Clone o repositorio 
>$ git clone [link do repositorio]

- Instale as dependencias 
>$ npm install

- Entre na pasta 'Game'
>$ cd game

- Rode o servidor 
> $ node server.js


- Vá até seu navegador e digite 'localhost:2000'